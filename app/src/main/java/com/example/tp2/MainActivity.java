package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;


public class MainActivity extends AppCompatActivity {

    WineDbHelper wineDbHelper=null;
    SimpleCursorAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        wineDbHelper = new WineDbHelper(this);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        if (wineDbHelper.fetchAllWines().getCount() == 0) {
            wineDbHelper.populate();
        }
        adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, wineDbHelper.fetchAllWines(), new String[]{
                wineDbHelper.COLUMN_NAME,
                wineDbHelper.COLUMN_WINE_REGION}, new int[]{android.R.id.text1, android.R.id.text2}, 0);

        final ListView wineList;
        wineList = (ListView) findViewById(R.id.listWine);
        registerForContextMenu(wineList);
        wineList.setAdapter(adapter);

        wineList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SQLiteDatabase db = wineDbHelper.getReadableDatabase();
                Cursor cursor = null;

                String selectionId = "_id=" + id;
                cursor = db.query(WineDbHelper.TABLE_NAME, null, selectionId, null, null, null, null);

                Wine wineSelect = null;
                if (cursor != null) {
                    wineSelect = WineDbHelper.cursorToWine(cursor);

                }
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                intent.putExtra("wine", wineSelect);
                startActivity(intent);
            }
        });


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Wine wineAdd = new Wine("Title par défaut", "Region par défaut", "Localisation par défaut", "Climat par défaut", "Surface par défaut");
                wineDbHelper.addWine(wineAdd);
                Cursor cursorAll=wineDbHelper.fetchAllWines();
                adapter.swapCursor(cursorAll);

                Cursor cursorWine = null;
                SQLiteDatabase db = wineDbHelper.getReadableDatabase();

                long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+wineDbHelper.TABLE_NAME, null);
                String rowID=Long.toString(numRows);
                Toast.makeText(MainActivity.this, "Ajout vin" + rowID, Toast.LENGTH_LONG).show();
                wineAdd.setId(numRows);
                Intent intentWineAdd = new Intent(MainActivity.this, WineActivity.class);
                intentWineAdd.putExtra("wine", wineAdd);
                startActivity(intentWineAdd);
            }
        });
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId()==R.id.listWine) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.layoutmenu, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId())
        {
            case R.id.buttonDelete:
                SQLiteDatabase db = wineDbHelper.getReadableDatabase();
                String selectionId = "_id=" + info.id;
                Cursor cursor = db.query(WineDbHelper.TABLE_NAME, null, selectionId, null, null, null, null);
                wineDbHelper.deleteWine(cursor);
                adapter.swapCursor(wineDbHelper.fetchAllWines());

                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }
}
