package com.example.tp2;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class WineActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        final WineDbHelper wineDbHelper = new WineDbHelper(this);
        final Wine wineSelect = (Wine) getIntent().getParcelableExtra("wine");

        ((EditText) findViewById(R.id.wineName)).setText(wineSelect.getTitle());
        ((EditText) findViewById(R.id.editWineRegion)).setText(wineSelect.getRegion());
        ((EditText) findViewById(R.id.editClimate)).setText(wineSelect.getClimate());
        ((EditText) findViewById(R.id.editLoc)).setText(wineSelect.getLocalization());
        ((EditText) findViewById(R.id.editPlantedArea)).setText(wineSelect.getPlantedArea());

        final Button button = findViewById(R.id.buttonSave);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Wine wineUpdate=new Wine();
                    wineUpdate.setId(wineSelect.getId());
                    wineUpdate.setTitle(((EditText) findViewById(R.id.wineName)).getText().toString());
                    wineUpdate.setRegion(((EditText) findViewById(R.id.editWineRegion)).getText().toString());
                    wineUpdate.setClimate(((EditText) findViewById(R.id.editClimate)).getText().toString());
                    wineUpdate.setLocalization(((EditText) findViewById(R.id.editLoc)).getText().toString());
                    wineUpdate.setPlantedArea(((EditText) findViewById(R.id.editPlantedArea)).getText().toString());
                    wineDbHelper.updateWine(wineUpdate);
                    Toast.makeText(WineActivity.this, "Sauvegarde des modifications", Toast.LENGTH_LONG).show();
                }
            });
    }
}
